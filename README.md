# Reproduccion de audio

En android, existe la posibilidad de incluir sonido a nuestras aplicaciones a traves de audios, ya
sean pequeños sonidos de corta duracion,o bien archivos de audio de larga duracion como puede ser una
cancion.

Para cualquiera que sea nuestra necesidad existen dos closes que nos permiten lograrlo:

- La clase MediaPlayer : Es utilizada para reproducir archivos de audios largos, es decir, para reproducir
                         música o grabaciones de audio de larga duracion.
- La clase SoundPool : Es utilizada para reproducir archivos de audio muy cortos, tales como efectos
                       de botones, y es importante tener en cuenta que el archivo de audio que reproduzca
                       esta clase, debe de tener un tamaño maximo de 1Mb.
- MediaRecorder : esta clase se utiliza para grabar audio , video
- para que nuestra aplicacion sea multilenguaje: dirigirnos en la carpera values de res (perspectiva android)
  luego cliquear new Values Resource file, poner el mismo strings sin la extension y elegir Locale
- ActionBar - Menú OverFlow : es un menu desplegable que muestra acciones u opciones
    para crearlo, lo pásos son igual al enterior pero con el nombre de nuestro menu (overflow) y en values (menu) ->o k
- los actionbuttons son elementos que se encontraran en el actionbar