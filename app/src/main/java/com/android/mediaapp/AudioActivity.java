package com.android.mediaapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AudioActivity extends AppCompatActivity {

    private MediaRecorder grabacion;
    private String archivoSalida = null;
    Button btnGrabar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

//        Código para la condición de los permisos dentro del método OnCreate:
        // verificando que los permisos se encuentren en el archivo manifest
        if (
                ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    AudioActivity.this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO
                    },
                    1000
            );
        }
        btnGrabar = findViewById(R.id.btnGrabar);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnReproducir})
    public void reproducir(){
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(this.archivoSalida);
            mediaPlayer.prepare();
        }catch (IOException e){

        }
        mediaPlayer.start();
        Toast.makeText(this,"Reproduciendo",Toast.LENGTH_LONG).show();
    }

    @OnClick({R.id.btnGrabar})
    public  void grabar(){
        if(grabacion == null){
            this.archivoSalida = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Grabacion.mp3";
            grabacion = new MediaRecorder();
            grabacion.setAudioSource(MediaRecorder.AudioSource.MIC); // usando el sensor
            grabacion.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            grabacion.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            grabacion.setOutputFile(archivoSalida);
            try {
                grabacion.prepare();
                grabacion.start();
            }catch (IOException ex){

            }
            btnGrabar.setBackgroundResource(R.drawable.rec);
            Toast.makeText(getApplicationContext(),"Grabando ...",Toast.LENGTH_LONG).show();
        }else if (grabacion != null){
            grabacion.stop();
            grabacion.release();
            grabacion = null;
            btnGrabar.setBackgroundResource(R.drawable.stop_rec);
            Toast.makeText(getApplicationContext(),"Grabacion finalizada",Toast.LENGTH_LONG).show();
        }
    }
}