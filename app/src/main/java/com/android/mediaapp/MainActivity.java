package com.android.mediaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    Button btn_soundPool;
    Button btn_mediaPlayer;

    SoundPool sp;
    int sonido_en_reproduccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        btn_soundPool = findViewById(R.id.soundPool);
//        btn_mediaPlayer = findViewById(R.id.mediaplayer);
        sp = new SoundPool(1, AudioManager.STREAM_MUSIC,1);
        sonido_en_reproduccion = sp.load(this,R.raw.sound_short,1);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.soundPool)
    public void reproducirMusicaCorta(){
        sp.play(sonido_en_reproduccion,1,1,1,0,0);
    }

    @OnClick(R.id.mediaplayer)
    public  void reproducirMusicaLarga(){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.sound_long);
        mp.start();
    }

    @OnClick(R.id.btnIrReproductor)
    public void irAlReproductor(View view){
        Intent intent = new Intent (this, ReproductorActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);

    }
    @OnClick(R.id.idAudio)
    public void irAlAudio(View view){
        Intent intent = new Intent (this, AudioActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);

    }
    @OnClick(R.id.btnIrLenguaje)
    public void irAlMultilenguaje(View view){
        Intent intent = new Intent (this, LenguajeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);

    }
    @OnClick(R.id.foto)
    public void irAlfoto(View view){
        Intent intent = new Intent (this, Camara.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);

    }

    @OnClick(R.id.grabar)
    public void grabar(View view){
        Intent intent = new Intent (this, Video.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }

    @OnClick(R.id.btnResponsive)
    public void responsive(View view){
        Intent intent = new Intent (this, ResponsiveDesign.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 0);
    }

    // para mostrar o ocultar el overflow del actionbar
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.overflow,menu);
        return true;
    }

    // metodo para asignar las funciones correspondientes a las opciones
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.item1:
                Toast.makeText(this,"item1",Toast.LENGTH_LONG).show();
                
                break;
            case R.id.item2 :
                Toast.makeText(this,"item2",Toast.LENGTH_LONG).show();
                break;
            case R.id.item3 :
                Toast.makeText(this,"item3",Toast.LENGTH_LONG).show();
                break;
            case R.id.shared:
                Toast.makeText(this,"shared",Toast.LENGTH_LONG).show();
                return true;
            case R.id.searchIcon :
                Toast.makeText(this,"searchIcon",Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}