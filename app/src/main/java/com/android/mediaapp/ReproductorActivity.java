package com.android.mediaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReproductorActivity extends AppCompatActivity {

    private int repetir = 2,posicion = 0;
    private MediaPlayer vectormp [] = new MediaPlayer[3];

    private Button btnPlay;
    private Button btnRepetir;
    private ImageView portada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reproductor);

        vectormp[0] = MediaPlayer.create(this,R.raw.race);
        vectormp[1] = MediaPlayer.create(this,R.raw.sound);
        vectormp[2] = MediaPlayer.create(this,R.raw.tea);

        btnPlay = findViewById(R.id.btnPlay);
        btnRepetir = findViewById(R.id.btnLoop);
        portada = findViewById(R.id.imgPortada);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnPlay})
    public void play(){
        if(vectormp[posicion].isPlaying()){
            vectormp[posicion].pause();
            btnPlay.setBackgroundResource(R.drawable.reproducir);
            Toast.makeText(this,"Pausa", Toast.LENGTH_SHORT).show();
        }else{
            vectormp[posicion].start();
            btnPlay.setBackgroundResource(R.drawable.pausa);
            Toast.makeText(this,"Reproduciendo", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.btnStop})
    public void stop(){
        if(vectormp[posicion] == null)
            return;
        vectormp[posicion].stop();
        posicion = 0;
        btnPlay.setBackgroundResource(R.drawable.reproducir);
        btnRepetir.setBackgroundResource(R.drawable.no_repetir);

        portada.setImageResource(R.drawable.portada1);
        vectormp[0] = MediaPlayer.create(this,R.raw.race);
        vectormp[1] = MediaPlayer.create(this,R.raw.sound);
        vectormp[2] = MediaPlayer.create(this,R.raw.tea);
    }


    @OnClick({R.id.btnLoop})
    public void loop(){
        Toast.makeText(this,repetir+"", Toast.LENGTH_SHORT).show();
        if(repetir == 1){
            btnRepetir.setBackgroundResource(R.drawable.no_repetir);
            vectormp[posicion].setLooping(false);
            repetir = 2;
        }else{
            repetir = 1;
            btnRepetir.setBackgroundResource(R.drawable.repetir);
            vectormp[posicion].setLooping(true);
        }

    }


    @OnClick({R.id.btnPre})
    public void pre(){
        if((posicion-1) < 0){
            posicion = 0;
            Toast.makeText(this,"No hay mas canciones", Toast.LENGTH_SHORT).show();
        }else{
            vectormp[posicion].pause();
            vectormp[--posicion].start();
            btnPlay.setBackgroundResource(R.drawable.pausa);
            switch (posicion){
                case 0 :
                    portada.setImageResource(R.drawable.portada1);
                    break;
                case 1 :
                    portada.setImageResource(R.drawable.portada2);
                    break;
                case 2:
                    portada.setImageResource(R.drawable.portada3);
                    break;
            }
        }
    }

    @OnClick({R.id.btnPos})
    public void pos(){
        if(posicion+1 >= vectormp.length){
            posicion = 2;
            Toast.makeText(this,"No hay mas canciones", Toast.LENGTH_SHORT).show();
        }else{
            vectormp[posicion].pause();
            vectormp[++posicion].start();
            btnPlay.setBackgroundResource(R.drawable.pausa);
            portada.setBackgroundResource(0);
            switch (posicion){
                case 0 :
                    portada.setImageResource(R.drawable.portada1);
                    break;
                case 1 :
                    portada.setImageResource(R.drawable.portada2);
                    break;
                case 2:
                    portada.setImageResource(R.drawable.portada3);
                    break;
            }
        }
    }
}