package com.android.mediaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class ResponsiveDesign extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_responsive_design);
    }
}